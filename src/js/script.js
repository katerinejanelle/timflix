document.addEventListener("DOMContentLoaded", function(event) {
    //Section Carousel
    var intervalId = 0;
    var scrollButton = document.querySelector('.fleche');

    function flecheHaut() {

        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function haut() {
        intervalId = setInterval(flecheHaut, 16.66);
    }

     scrollButton.addEventListener('click', haut);




    var connexion = new MovieDb();

if (document.location.pathname.search("fiche-film.html") > 0)
{
    //Dans la page fiche-film.html
    console.log(document.location);

    var params = (new URL(document.location)).searchParams;

    connexion.requeteInfoFilm(params.get("id"));
    connexion.requeteInfoActeur(params.get("id"));
}
else {
    //Dans la page index.html
    connexion.requetePopulaireFilms();
    connexion.requetePresentFilms();

    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
        },
    });

}
});



class MovieDb {

    constructor() {

        this.APIkey = "1e52fd90df5098eaa6d88b48312a9c57";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185", "original"];

        this.totalFilm = 6;

        this.totalFilmPresent = 9;

        this.totalActeur = 6;

        this.idMovie = "fiche-film.html?id=";

    }

    requetePopulaireFilms() {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequetePopulaireFilms.bind(this));

        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }


    retourRequetePopulaireFilms(event) {

        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {


            data = JSON.parse(target.responseText).results;

            this.afficherPopulaireFilms(data);
        }
    }


    afficherPopulaireFilms(data) {

        for (var i = 0; i < this.totalFilm; i++) {


            //Clone de l'article dans le template
            var unArticle = document.querySelector(".film").cloneNode(true);


            unArticle.querySelector("h2").innerText = data[i].title;

            function reduireTexte (element, nbrMaxMot, sufix) {

                if (!element || !nbrMaxMot) return;

                var texte = element.textContent;

                texte = texte.split(' ');

                if(texte.length < nbrMaxMot) return;

                texte = texte.slice(0, nbrMaxMot);

                texte = texte.join(' ') + (sufix ? sufix : '');

                element.textContent = texte;


                if (data[i].overview == "") {
                    unArticle.querySelector(".description").innerText = "Sans descritpion";
                } else {
                    unArticle.querySelector(".description").innerText = texte;
                }

            }

// Nous sélectionnons tous les éléments avec la classe '.reduire'.
            var elements = document.querySelector(".reduire");
            elements.innerText = data[i].overview;

// Pour chaque élément sélectionné, nous appelons la fonction

            reduireTexte(elements, 10, '...');


            unArticle.querySelector("p.annee").innerText = data[i].release_date;

            unArticle.querySelector("p.rating").innerText = data[i].vote_average;

            var uneImage = unArticle.querySelector("img");

            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurAffiche[4] + data[i].poster_path);

            var lienFilm = document.querySelector("a.lienFilm");

            lienFilm.setAttribute("href", this.idMovie + data[i + 1].id);

            var lienFilmAffiche = document.querySelector("a.lienFilmAffiche");

            lienFilmAffiche.setAttribute("href", this.idMovie + data[i + 1].id);

            //Ajout du clone dans la liste de films
            document.querySelector("section.main").appendChild(unArticle);
        }
    }

        requetePresentFilms() {

            var xhr = new XMLHttpRequest();

            xhr.addEventListener("readystatechange", this.retourRequetePresentFilms.bind(this));

            xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

            xhr.send();
        }


        retourRequetePresentFilms(event) {

            var target = event.currentTarget;

            var data;

            if (target.readyState === target.DONE) {


                data = JSON.parse(target.responseText).results;

                this.afficherPresentFilms(data);
            }
        }


        afficherPresentFilms(data) {

            for (var i = 0; i < this.totalFilmPresent; i++) {


                //Clone de l'article dans le template
                var unePage = document.querySelector(".swiper-slide").cloneNode(true);

                unePage.querySelector("h2").innerText = data[i].title;

                unePage.querySelector("p").innerText = data[i].vote_average;


                var uneImage = unePage.querySelector("img");

                uneImage.setAttribute("src", this.imgPath + this.largeurTeteAffiche[2] + data[i].backdrop_path);

                var lienFilmSwiper = document.querySelector("a.lienFilmSwiper");

                lienFilmSwiper.setAttribute("href", this.idMovie + data[i +1].id);


                //Ajout du clone dans la liste de films
                document.querySelector(".swiper-wrapper").appendChild(unePage);

            }

    }

    requeteInfoFilm(id) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this));

        xhr.open("GET", this.baseURL + "movie/" + id + "?language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();
    }

    retourRequeteInfoFilm(event) {

        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);
            this.afficheInfoFilm(data);
        }

    }

    afficheInfoFilm(data){

        var uneImage = document.querySelector(".poster");

        uneImage.setAttribute("src", this.imgPath + this.largeurTeteAffiche[2] + data.backdrop_path);

        document.querySelector('.titre-film').innerText = data.title;

        document.querySelector('.dateParution').innerText = data.release_date;

        if (data.overview == "") {
            document.querySelector(".description-film").innerText = "Sans descritpion";
        } else {
            document.querySelector(".description-film").innerText = data.overview;
        }

        document.querySelector('.langue-film').innerText = data.original_language;
        document.querySelector('.vote-film').innerText = data.vote_average;
        document.querySelector('.duree-film').innerText = data.runtime + " minutes";
        document.querySelector('.budget-film').innerText =  data.budget + "$";
        document.querySelector('.recette').innerText = data.revenue + "$";


    }


    requeteInfoActeur(id) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfoActeur.bind(this));

        xhr.open("GET",  this.baseURL + "movie/" + id + "/credits?api_key=" + this.APIkey);

        xhr.send();
    }

    retourRequeteInfoActeur(event) {

        var target = event.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);
            this.afficheInfoActeur(data);

        }

    }

    afficheInfoActeur(data) {

        console.log(data["cast"][3]);
        for (var i = 0; i < this.totalActeur; i++) {

            //Clone de l'article dans le template
            var unePageActeur = document.querySelector(".credit-acteur").cloneNode(true);


            var uneImage = unePageActeur.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.largeurTeteAffiche[1] + data["cast"][i].profile_path);

            unePageActeur.querySelector(".nomActeur").innerText = data["cast"][i].name;

            //Ajout du clone dans la liste de films
            document.querySelector(".acteur").appendChild(unePageActeur);

        }

    }
}
